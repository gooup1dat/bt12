

document.addEventListener("DOMContentLoaded", function() {
    function validateDepartmentSelected() {
        var department = document.getElementById("department");
        if (department.value === "000") {
            alert("Please select a department");
            return false;
        }
        return true;
    }

    document.getElementById("form").addEventListener("submit", function() {
        event.preventDefault();
        var studentCode = document.getElementById("student-code").value;
        var name = document.getElementById("name").value;
        var email = document.getElementById("email").value;
        var phone = document.getElementById("phone").value;
        var dob = document.getElementById("birthday").value;
        var gender = document.querySelector('input[name="gender"]:checked').value;
        var department = document.getElementById("department");
        var clazz = document.getElementById("clazz").value;
        
        var data = {
            studentCode,
            name,
            email,
            phone,
            dob,
            gender,
            clazz,
        }

        if(!validateDepartmentSelected()) {
            return;
        }
        
        data.department = department.options[department.selectedIndex].text;
        
        console.log(data);
    });

});

